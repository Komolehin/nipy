import pytest
import numpy as np
from nipy.testing import anatfile
from nipy import load_image
from nipy import save_image
from nipy.core.api import Image, vox2mni

# Reference for nipy example usage, visit official documentation
# https://nipy.org/nipy/users/basic_io.html

# fixture
@pytest.fixture
def image_from_disk():
    return load_image(anatfile)

# Load Image from File
def test_loaded_image_shape(image_from_disk):
    assert image_from_disk.shape == (33, 41, 25)

# Access Data into an Array
def test_loaded_image_dimension(image_from_disk):
    assert image_from_disk.ndim == 3

# Save image to a File
def test_image_save_to_file(tmp_path, image_from_disk):
    output_file = tmp_path / 'newimagefile.nii'
    newimg = save_image(image_from_disk, str(output_file))
    assert output_file.is_file()


# Create Image from an Array
def test_create_image_from_array():
    rawarray = np.zeros((43,128,128))
    arr_img = Image(rawarray, vox2mni(np.eye(4)))
    assert arr_img.shape == (43,128,128)